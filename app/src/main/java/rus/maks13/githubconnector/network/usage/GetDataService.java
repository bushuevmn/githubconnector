package rus.maks13.githubconnector.network.usage;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rus.maks13.githubconnector.model.Follower;
import rus.maks13.githubconnector.model.Repository;
import rus.maks13.githubconnector.model.User;
import rx.Observable;

public interface GetDataService {

   @GET("/user/followers")
   Observable<List<Follower>> getFollowers();

   @GET("/user")
   Observable<User> getMyProfileInfo();

   @GET("/users/{user}")
   Observable<Follower> getAdditionUserInfo(@Path("user") String user);

   @GET("/users/{user}/starred?per_page=1")
   Call<List<Follower>> getStarredRepositoriesUser(@Path("user") String user);

   @GET("/users/{user}/repos")
   Observable<List<Repository>> getRepositoriesInfo(@Path("user") String user);

}
