package rus.maks13.githubconnector.network.usage;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rus.maks13.githubconnector.db.model.Token;
import rus.maks13.githubconnector.model.TokenPermission;
import rus.maks13.githubconnector.model.User;
import rx.Observable;

public interface LoginService {
   @GET("/user")
   Observable<User> basicLogin();

   @POST("/authorizations")
   Observable<Token> getAccessTokenByScope(
      @Body TokenPermission tokenPermission);
}
