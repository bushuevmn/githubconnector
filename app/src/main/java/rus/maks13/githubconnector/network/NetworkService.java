package rus.maks13.githubconnector.network;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.activeandroid.query.Select;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rus.maks13.githubconnector.db.model.Token;
import rus.maks13.githubconnector.network.usage.GetDataService;
import rus.maks13.githubconnector.utilites.Constants;
import rus.maks13.githubconnector.utilites.DataBaseHelper;

public class NetworkService {

   private static final String TAG = "NetworkService";

   private static GetDataService mGetDataService;
   private static GetDataService mGetStarredService;

   private static OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
      .readTimeout(3, TimeUnit.SECONDS)
      .connectTimeout(3, TimeUnit.SECONDS);

   private static Retrofit.Builder builder = new Retrofit.Builder()
      .baseUrl(Constants.API_BASIC_URL)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

   private static Retrofit.Builder builderStarred = new Retrofit.Builder()
      .baseUrl(Constants.API_BASIC_URL)
      .addConverterFactory(GsonConverterFactory.create());

   /*public static <S> S createService(Class<S> serviceClass) {
      Retrofit retrofit = builder.client(okHttpClient.build()).build();
      return retrofit.create(serviceClass);
   }*/

   public static void clearClient() {
      okHttpClient = new OkHttpClient.Builder()
         .readTimeout(3, TimeUnit.SECONDS)
         .connectTimeout(3, TimeUnit.SECONDS);

      builder = new Retrofit.Builder()
         .baseUrl(Constants.API_BASIC_URL)
         .addConverterFactory(GsonConverterFactory.create())
         .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
   }

   public static <S> S createService(Class<S> serviceClass, String userName, String password) {
      if (userName != null && password != null) {
         String credentials = userName + ":" + password;
         final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(),
            Base64.NO_WRAP);

         okHttpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Log.d(TAG, basic);

            Request.Builder requestBuilder = original.newBuilder()
               .header("Authorization", basic)
               .header("Accept", "*/*")
               .method(original.method(), original.body());

            Request request = requestBuilder.build();
            return chain.proceed(request);
         });
      }

      OkHttpClient client = okHttpClient.build();
      Retrofit retrofit = builder.client(client).build();
      return retrofit.create(serviceClass);

   }

   public static <S> S createService(Class<S> serviceClass, String token) {
      if (token != null) {
         okHttpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder requestBuilder = original.newBuilder()
               .header("Accept", "*/*")
               .header("Authorization", "token "
                  + token)
               .header("client_id", Constants.CLIENT_ID)
               .header("client_secret", Constants.CLIENT_SECRET)
               .method(original.method(), original.body());

            Request request = requestBuilder.build();
            return chain.proceed(request);
         });
      }

      OkHttpClient client = okHttpClient.build();
      Retrofit retrofit = builder.client(client).build();
      return retrofit.create(serviceClass);
   }

   public static <S> S createServiceWorkCall(Class<S> serviceClass, String token) {
      if (token != null) {
         okHttpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder requestBuilder = original.newBuilder()
               .header("Accept", "*/*")
               .header("Authorization", "token "
                  + token)
               .header("client_id", Constants.CLIENT_ID)
               .header("client_secret", Constants.CLIENT_SECRET)
               .method(original.method(), original.body());

            Request request = requestBuilder.build();
            return chain.proceed(request);
         });
      }

      OkHttpClient client = okHttpClient.build();
      Retrofit retrofit = builderStarred.client(client).build();
      return retrofit.create(serviceClass);
   }

   private static void createGetStarredService(String token) {
      mGetStarredService = createServiceWorkCall(GetDataService.class, token);
   }

   public static GetDataService getStarredService(Context context) {
      if (mGetStarredService == null) {
         clearClient();
         createGetStarredService(getToken(context));
      }
      return mGetStarredService;
   }


   private static void createGetDataService(String token) {
      mGetDataService = createService(GetDataService.class, token);
   }

   public static GetDataService getDataService(Context context) {
      if (mGetDataService == null) {
         clearClient();
         createGetDataService(getToken(context));
      }
      return mGetDataService;
   }

   private static String getToken(Context context) {
      DataBaseHelper.connectToDb(context);
      Token token = new Select().from(Token.class).executeSingle();
      return token.getToken();
   }
}
