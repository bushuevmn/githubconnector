package rus.maks13.githubconnector.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.adapters.UserInfoAdapter;
import rus.maks13.githubconnector.model.Follower;
import rus.maks13.githubconnector.model.Repository;
import rus.maks13.githubconnector.network.NetworkService;
import rus.maks13.githubconnector.utilites.Constants;
import rus.maks13.githubconnector.utilites.FragmentController;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UserInfoFragment extends Fragment {

   private static final String TAG = "UserInfoFrm";
   private ArrayList<Repository> mRepositories = new ArrayList<>();
   private Follower mFollower;
   private UserInfoAdapter mUserInfoAdapter;
   private RecyclerView listRepositories;

   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState) {
      getFollowerInfo();
      getRepositoryInfo();
      View view = inflater.inflate(R.layout.frm_user_info, container, false);
      listRepositories = (RecyclerView) view.findViewById(R.id.frm_user_info_rv_repositories);
      mUserInfoAdapter = new UserInfoAdapter(getContext(), mFollower, mRepositories);
      LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
      listRepositories.setLayoutManager(layoutManager);

      return view;
   }

   @Override
   public void onResume() {
      super.onResume();
      FragmentController.getInstance().setTitle(Constants.FRAGMENT_USER_INFO);
   }

   private void getFollowerInfo() {
      Bundle bundle = getArguments();
      mFollower = bundle.getParcelable(Constants.PASS_FOLLOWER_INFO);
      //Log.d(TAG, mFollower.toString());
   }

   private void getRepositoryInfo() {
      mRepositories.clear();
      mRepositories.add(new Repository());
      NetworkService.getDataService(getContext())
         .getRepositoriesInfo(mFollower.getLogin())
         .observeOn(AndroidSchedulers.mainThread())
         .subscribeOn(Schedulers.newThread())
         .subscribe(new Subscriber<List<Repository>>() {
            @Override
            public void onCompleted() {
               Log.d(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
               e.printStackTrace();
            }

            @Override
            public void onNext(List<Repository> repositories) {
               Log.d(TAG, "onNext");
               mRepositories.addAll(repositories);
               listRepositories.setAdapter(mUserInfoAdapter);
               mUserInfoAdapter.notifyDataSetChanged();
               Log.d(TAG, mRepositories.toString());
            }
         });
   }
}
