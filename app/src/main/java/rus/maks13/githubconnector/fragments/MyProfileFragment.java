package rus.maks13.githubconnector.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.IOException;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.model.Follower;
import rus.maks13.githubconnector.model.User;
import rus.maks13.githubconnector.network.NetworkService;
import rus.maks13.githubconnector.utilites.Constants;
import rus.maks13.githubconnector.utilites.FragmentController;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class MyProfileFragment extends Fragment {

   private static final String TAG = "MyProfileFrm";

   private DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
      .showImageForEmptyUri(R.drawable.avatar_holder)
      .showImageOnFail(R.drawable.avatar_holder)
      .resetViewBeforeLoading(true)
      .cacheOnDisk(true)
      .imageScaleType(ImageScaleType.EXACTLY)
      .bitmapConfig(Bitmap.Config.RGB_565)
      .considerExifParams(true)
      .displayer(new FadeInBitmapDisplayer(150))
      .build();
   private ImageView avatarImageView;
   private TextView loginTextView;
   private TextView nameTextView;
   private TextView companyTextView;
   private TextView cityTextView;
   private TextView joinedTextView;
   private TextView followersTextView;
   private TextView starredTextView;
   private TextView followingTextView;
   private TextView userTypeTextView;
   private TextView eMailTextView;
   private TextView diskUsageTextView;
   private TextView bioTextView;
   private User mUser;

   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.frm_my_profile, container, false);

      avatarImageView = (ImageView) view.findViewById(R.id.frm_my_profile_iv_avatar);
      loginTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_login);
      nameTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_name);
      companyTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_company);
      cityTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_city);
      joinedTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_joined);
      followersTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_followers);
      starredTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_starred);
      followingTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_following);
      userTypeTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_user_type);
      eMailTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_e_mail);
      diskUsageTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_disk_usage);
      bioTextView = (TextView) view.findViewById(R.id.frm_my_profile_tv_bio);

      getUserInfo();

      return view;
   }

   @Override
   public void onResume() {
      super.onResume();
      FragmentController.getInstance().setTitle(Constants.FRAGMENT_MY_PROFILE);
   }

   private void getUserInfo() {
      mUser = new User();
      NetworkService.getDataService(getContext())
         .getMyProfileInfo()
         .single(user -> {
            Log.d(TAG, "User info in single\n" + user.toString());
            Call<List<Follower>> followerResponse = NetworkService.getStarredService(getContext())
               .getStarredRepositoriesUser(user.getLogin());
            Headers headers = getHeader(followerResponse);
            if (followerResponse != null && headers != null) {
               String link = headers.get("Link");
               if (link != null && link.contains("rel=\"last\"")) {
                  Log.d(TAG, headers.toString());
                  Log.d(TAG, link);
                  int startPosition = link.lastIndexOf("&page=") + 6;
                  int endPosition = link.lastIndexOf(">;");
                  int starred = Integer.valueOf(
                     link.substring(startPosition, endPosition));
                  user.setStarredRepositories(starred);
                  Log.d(TAG, "starred=" + starred);
               } else {
                  Log.d(TAG, "Link is null!");
               }
            } else {
               Log.d(TAG, "Response is null!");
            }
            return true;
         }).observeOn(AndroidSchedulers.mainThread())
         .subscribeOn(Schedulers.newThread())
         .subscribe(new Subscriber<User>() {
            @Override
            public void onCompleted() {
               Log.d(TAG, "onCompleted");
               showProfileData();
            }

            @Override
            public void onError(Throwable e) {
               e.printStackTrace();
            }

            @Override
            public void onNext(User user) {
               Log.d(TAG, user.toString());
               mUser = user;
            }
         });
   }

   private Headers getHeader(Call<List<Follower>> followerResponse) {
      Headers headers = null;
      try {
         headers = followerResponse.execute().headers();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return headers;
   }

   private void showProfileData() {
      ImageLoader.getInstance().displayImage(mUser.getAvatarUrl(), avatarImageView, mOptions);
      loginTextView.setText(mUser.getLogin());
      if (mUser.getName() != null && !mUser.getName().isEmpty())
         nameTextView.setText(mUser.getName());
      else
         nameTextView.setText(getResources().getString(R.string.text_holder_not_specified));
      if (mUser.getCompany() != null && !mUser.getCompany().isEmpty())
         companyTextView.setText(mUser.getCompany());
      else
         companyTextView.setText(getResources().getString(R.string.text_holder_not_specified));
      if (mUser.getCity() != null && !mUser.getCity().isEmpty())
         cityTextView.setText(mUser.getCity());
      else
         cityTextView.setText(getResources().getString(R.string.text_holder_not_specified));
      joinedTextView.setText(mUser.getCreatedProfile());
      followersTextView.setText(String.valueOf(mUser.getFollowers()));
      starredTextView.setText(String.valueOf(mUser.getStarredRepositories()));
      followingTextView.setText(String.valueOf(mUser.getFollowing()));
      userTypeTextView.setText(mUser.getUserType());
      if (mUser.getEmail() != null && !mUser.getEmail().isEmpty())
         eMailTextView.setText(mUser.getEmail());
      else
         eMailTextView.setText(getResources().getString(R.string.text_holder_not_specified));
      diskUsageTextView.setText(String.valueOf(mUser.getDiskUsage()));
      if (mUser.getBio() != null && !mUser.getBio().isEmpty())
         bioTextView.setText(mUser.getBio());
      else
         bioTextView.setText(getResources().getString(R.string.text_holder_not_specified));
   }
}
