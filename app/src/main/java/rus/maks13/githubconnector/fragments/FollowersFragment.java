package rus.maks13.githubconnector.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.adapters.FollowersAdapter;
import rus.maks13.githubconnector.model.Follower;
import rus.maks13.githubconnector.network.NetworkService;
import rus.maks13.githubconnector.utilites.Constants;
import rus.maks13.githubconnector.utilites.FragmentController;
import rus.maks13.githubconnector.utilites.RecyclerItemClickListener;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FollowersFragment extends Fragment {

   private static final String TAG = "FollowersFrm";
   private ArrayList<Follower> followers = new ArrayList<>();
   private FollowersAdapter mFollowersAdapter;
   private RecyclerView mListFollowers;

   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                            @Nullable Bundle savedInstanceState) {
      Log.d(TAG, "onCreateView");
      View view = inflater.inflate(R.layout.frm_list_following, container, false);
      mListFollowers = (RecyclerView) view.findViewById(R.id.frm_list_following_rv_following);
      mFollowersAdapter = new FollowersAdapter(getContext(), followers);
      LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
      mListFollowers.setLayoutManager(layoutManager);

      mListFollowers.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
         (view1, position) -> {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.PASS_FOLLOWER_INFO, followers.get(position));
            UserInfoFragment userInfoFragment = new UserInfoFragment();
            userInfoFragment.setArguments(bundle);
            FragmentController.getInstance().pushFragmentToStack(userInfoFragment,
               Constants.FRAGMENT_USER_INFO);
         }));

      getFollowers();
      return view;
   }

   @Override
   public void onResume() {
      super.onResume();
      FragmentController.getInstance().setTitle(Constants.FRAGMENT_FOLLOWERS);
   }

   private void getFollowers() {
      Log.d(TAG, "getFollowers");
      followers.clear();
      NetworkService.getDataService(getContext()).getFollowers()
         .flatMapIterable(followers -> {
            for (Follower follower :
               followers) {
               NetworkService.getDataService(getContext())
                  .getAdditionUserInfo(follower.getLogin())
                  .toBlocking()
                  .subscribe(new Subscriber<Follower>() {
                     @Override
                     public void onCompleted() {
                        Log.d(TAG, "Second observable onCompleted()");
                     }

                     @Override
                     public void onError(Throwable e) {
                        e.printStackTrace();
                     }

                     @Override
                     public void onNext(Follower additionInfoFollower) {
                        Log.d(TAG, "Second observable");
                        Log.d(TAG, additionInfoFollower.toString());
                        follower.setAvatar(additionInfoFollower.getAvatar());
                        follower.setFollowers(additionInfoFollower.getFollowers());
                        follower.setFollowing(additionInfoFollower.getFollowing());
                        follower.setCompany(additionInfoFollower.getCompany());
                        follower.setName(additionInfoFollower.getName());
                        follower.setCity(additionInfoFollower.getCity());
                        follower.setJoined(additionInfoFollower.getJoined());

                        Call<List<Follower>> followerResponse = NetworkService.getStarredService(getContext())
                           .getStarredRepositoriesUser(follower.getLogin());
                        Headers headers = getHeader(followerResponse);
                        if (followerResponse != null && headers != null) {
                           String link = headers.get("Link");
                           if (link != null && link.contains("rel=\"last\"")) {
                              Log.d(TAG, headers.toString());
                              Log.d(TAG, link);
                              int startPosition = link.lastIndexOf("&page=") + 6;
                              int endPosition = link.lastIndexOf(">;");
                              int starred = Integer.valueOf(
                                 link.substring(startPosition, endPosition));
                              follower.setStarredRepositories(starred);
                              Log.d(TAG, "starred=" + starred);
                           }
                        }
                     }
                  });
            }
            return followers;
         }).observeOn(AndroidSchedulers.mainThread())
         .subscribeOn(Schedulers.newThread())
         .subscribe(new Subscriber<Follower>() {
            @Override
            public void onCompleted() {
               Log.d(TAG, "First observable onCompleted");
               mListFollowers.setAdapter(mFollowersAdapter);
               mFollowersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Throwable e) {
               e.printStackTrace();
            }

            @Override
            public void onNext(Follower follower) {
               Log.d(TAG, "First observable onNext()");
               Log.d(TAG, follower.toString());
               followers.add(follower);
            }
         });
   }

   private Headers getHeader(Call<List<Follower>> followerResponse) {
      Headers headers = null;
      try {
         headers = followerResponse.execute().headers();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return headers;
   }
}
