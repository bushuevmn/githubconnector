package rus.maks13.githubconnector.db.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

@Table(name = "token", id = "id")
public class Token extends Model {

   @SerializedName("id")
   @Column(name = "id")
   long id;

   @SerializedName("url")
   @Column(name = "url")
   String url;

   @SerializedName("token")
   @Column(name = "token")
   String token = "";

   @SerializedName("hashed_token")
   @Column(name = "hashed_token")
   String hashedToken;

   @SerializedName("token_last_eight")
   @Column(name = "token_last_eight")
   String tokenLastEight;

   @SerializedName("note")
   @Column(name = "note")
   String note;

   @SerializedName("created_at")
   @Column(name = "created_at")
   String createdAt;

   @SerializedName("updated_at")
   @Column(name = "updated_at")
   String updatedAt;

   public String getToken() {
      return token;
   }
}
