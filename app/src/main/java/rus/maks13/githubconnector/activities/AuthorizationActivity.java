package rus.maks13.githubconnector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.query.Delete;

import java.util.ArrayList;
import java.util.Collections;

import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.db.model.Token;
import rus.maks13.githubconnector.model.TokenPermission;
import rus.maks13.githubconnector.model.User;
import rus.maks13.githubconnector.network.NetworkService;
import rus.maks13.githubconnector.network.usage.LoginService;
import rus.maks13.githubconnector.utilites.Constants;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthorizationActivity extends AppCompatActivity {

   private static final String TAG = "AuthAc";

   private LoginService mLoginService;
   private EditText loginEditText;
   private EditText passwordEditText;
   private User mUser;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.ac_authorization);

      loginEditText = (EditText) findViewById(R.id.ac_authorization_edt_login);
      passwordEditText = (EditText) findViewById(R.id.ac_authorization_edt_password);
      Button loginButton = (Button) findViewById(R.id.ac_authorization_btn_login);
      loginButton.setOnClickListener(logInClickListener);
   }

   View.OnClickListener logInClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
         NetworkService.clearClient();
         mLoginService = NetworkService.createService(LoginService.class,
            loginEditText.getText().toString(),
            passwordEditText.getText().toString());

         getToken();
      }
   };


   private void getToken() {
      mLoginService.basicLogin()
         .observeOn(AndroidSchedulers.mainThread())
         .subscribeOn(Schedulers.newThread())
         .subscribe(new Subscriber<User>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
               Toast.makeText(getApplicationContext(),
                  "Error authorization", Toast.LENGTH_LONG).show();
               e.printStackTrace();
            }

            @Override
            public void onNext(User user) {

               ArrayList<String> scopeList = new ArrayList<>();
               Collections.addAll(scopeList, "user", "public_repo");
               TokenPermission tokenPermission =
                  new TokenPermission(scopeList, "Test app",
                     Constants.CLIENT_ID, Constants.CLIENT_SECRET);

               mLoginService.getAccessTokenByScope(tokenPermission)
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribeOn(Schedulers.newThread())
                  .subscribe(new Subscriber<Token>() {
                     @Override
                     public void onCompleted() {

                     }

                     @Override
                     public void onError(Throwable e) {
                        e.printStackTrace();
                     }

                     @Override
                     public void onNext(Token token) {
                        Log.d(TAG, "Token is get! " + token.toString());
                        new Delete().from(Token.class).execute();
                        token.save();
                        getUserInfo();
                     }
                  });
            }
         });
   }

   private void getUserInfo() {
      NetworkService.getDataService(getApplicationContext())
         .getMyProfileInfo()
         .observeOn(AndroidSchedulers.mainThread())
         .subscribeOn(Schedulers.newThread())
         .subscribe(new Subscriber<User>() {
            @Override
            public void onCompleted() {
               getNextActivity(MainActivity.class);
            }

            @Override
            public void onError(Throwable e) {
               Toast.makeText(getApplicationContext(),
                  "Error get user information, try authorized again",
                  Toast.LENGTH_LONG).show();
               e.printStackTrace();
            }

            @Override
            public void onNext(User user) {
               new Delete().from(User.class).execute();
               user.save();
               mUser = user;
            }
         });
   }

   private void getNextActivity(Class clazz) {
      Intent intent = new Intent(this, clazz);
      startActivity(intent);
      finish();
   }
}
