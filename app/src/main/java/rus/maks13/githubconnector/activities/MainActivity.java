package rus.maks13.githubconnector.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import de.hdodenhof.circleimageview.CircleImageView;
import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.db.model.Token;
import rus.maks13.githubconnector.fragments.FollowersFragment;
import rus.maks13.githubconnector.fragments.MyProfileFragment;
import rus.maks13.githubconnector.model.User;
import rus.maks13.githubconnector.utilites.Constants;
import rus.maks13.githubconnector.utilites.FragmentController;

public class MainActivity extends AppCompatActivity {

   private static final String TAG = "MainAc";
   private Toolbar mToolbar;
   private static TextView mTitle;
   private ActionBarDrawerToggle mDrawerToggle;
   private DrawerLayout mDrawerLayout;
   private NavigationView mNavigationView;
   private User mUser;
   private boolean isDataUpdate = true;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.ac_main);

      initImageLoader();
      Log.d(TAG, "onCreate");

      getUserInfo();

      mNavigationView = (NavigationView) findViewById(R.id.ac_main_nv_menu);
      mDrawerLayout = (DrawerLayout) findViewById(R.id.ac_main_dl_drawer);
      mToolbar = (Toolbar) findViewById(R.id.action_bar_tb_toolbar);
      setSupportActionBar(mToolbar);
      getSupportActionBar().setDisplayShowTitleEnabled(false);

      mNavigationView.setNavigationItemSelectedListener(mNavigationClickListener);
      mTitle = (TextView) findViewById(R.id.action_bar_tv_title);

      mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
         R.string.drawer_open, R.string.drawer_close) {
         public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
            invalidateOptionsMenu();
         }

         public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            if (isDataUpdate)
               showUserInfo();
            invalidateOptionsMenu();
         }
      };

      mDrawerToggle.setToolbarNavigationClickListener(v -> {
         if (FragmentController.getInstance().getFragmentManager()
            .getBackStackEntryCount() > 1) {
            FragmentController.getInstance().getActivity().onBackPressed();
         }
      });

      mDrawerLayout.setDrawerListener(mDrawerToggle);
      mDrawerToggle.syncState();


      FragmentController.initFragmentController(this, mTitle, mDrawerToggle);
      MyProfileFragment myProfileFragment = new MyProfileFragment();
      FragmentController.getInstance().clearStackAndPushFragment(myProfileFragment,
         Constants.FRAGMENT_MY_PROFILE);
   }

   @Override
   public void onBackPressed() {
      if (FragmentController.getInstance().getFragmentManager().getBackStackEntryCount() == 1) {
         finish();
      } else {
         super.onBackPressed();
      }
   }

   @Override
   protected void onNewIntent(Intent intent) {
      super.onNewIntent(intent);
   }

   @Override
   public void onLowMemory() {
      super.onLowMemory();
      ImageLoader.getInstance().clearMemoryCache();
      ImageLoader.getInstance().clearDiscCache();
   }

   @Override
   protected void onPostCreate(Bundle savedInstanceState) {
      super.onPostCreate(savedInstanceState);
      mDrawerToggle.syncState();
   }

   @Override
   public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      mDrawerToggle.onConfigurationChanged(newConfig);
   }

   NavigationView.OnNavigationItemSelectedListener mNavigationClickListener
      = new NavigationView.OnNavigationItemSelectedListener() {


      @Override
      public boolean onNavigationItemSelected(MenuItem view) {
         mDrawerLayout.closeDrawers();
         view.setChecked(false);
         switch (view.getItemId()) {
            case R.id.menu_items_it_followers: {
               FollowersFragment followersFragment = new FollowersFragment();
               FragmentController.getInstance().clearStackAndPushFragment(followersFragment,
                  Constants.FRAGMENT_FOLLOWERS);
               return true;
            }
            case R.id.menu_items_it_logout: {
               new Delete().from(Token.class).execute();
               new Delete().from(User.class).execute();
               Intent intent = new Intent(getApplicationContext(), AuthorizationActivity.class);
               startActivity(intent);
               finish();
               return true;
            }
            default: {
               return true;
            }
         }
      }
   };

   private void initImageLoader() {
      Log.d(TAG, "Init ImageLoader");
      ImageLoader.getInstance().init(ImageLoaderConfiguration.
         createDefault(getApplicationContext()));
   }

   private void getUserInfo() {
      mUser = new Select().from(User.class).executeSingle();
   }

   private void showUserInfo() {
      CircleImageView circleImageView = (CircleImageView) findViewById(R.id.header_menu_civ_avatar);
      TextView loginTextView = (TextView) findViewById(R.id.header_menu_tv_login);
      DisplayImageOptions options = new DisplayImageOptions.Builder()
         .showImageForEmptyUri(R.drawable.avatar_holder)
         .showImageOnFail(R.drawable.avatar_holder)
         .resetViewBeforeLoading(true)
         .cacheOnDisk(true)
         .imageScaleType(ImageScaleType.EXACTLY)
         .bitmapConfig(Bitmap.Config.RGB_565)
         .considerExifParams(true)
         .displayer(new FadeInBitmapDisplayer(150))
         .build();
      View view = findViewById(R.id.header_menu_vw_login);
      view.setOnClickListener(loginClickListener);
      loginTextView.setText(mUser.getLogin());
      ImageLoader.getInstance().displayImage(mUser.getAvatarUrl(), circleImageView, options);
      isDataUpdate = false;
   }

   View.OnClickListener loginClickListener = v -> {
      MyProfileFragment myProfileFragment = new MyProfileFragment();
      FragmentController.getInstance().clearStackAndPushFragment(myProfileFragment,
         Constants.FRAGMENT_MY_PROFILE);
      mDrawerLayout.closeDrawers();
   };
}
