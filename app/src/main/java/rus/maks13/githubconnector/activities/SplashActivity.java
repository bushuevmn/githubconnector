package rus.maks13.githubconnector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.activeandroid.query.Select;
import com.activeandroid.util.Log;

import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.db.model.Token;
import rus.maks13.githubconnector.model.User;
import rus.maks13.githubconnector.network.NetworkService;
import rus.maks13.githubconnector.network.usage.LoginService;
import rus.maks13.githubconnector.utilites.DataBaseHelper;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SplashActivity extends AppCompatActivity {

   private static final String TAG = "SplashAc";

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.ac_splash);

      checkToken();
   }

   private void checkToken() {
      DataBaseHelper.connectToDb(getApplicationContext());
      Token token = new Select().from(Token.class).executeSingle();
      if (token!=null && !token.getToken().equals("")) {
         NetworkService.clearClient();
         LoginService loginService = NetworkService.createService(LoginService.class, token.getToken());
         loginService.basicLogin()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(new Subscriber<User>() {
               @Override
               public void onCompleted() {

               }

               @Override
               public void onError(Throwable e) {
                  Log.d(TAG, "Not valid token");
                  getNextActivity(AuthorizationActivity.class);
               }

               @Override
               public void onNext(User user) {
                  Log.d(TAG, "Token is valid");
                  getNextActivity(MainActivity.class);
               }
            });
      } else {
         Log.d(TAG, "Not find token in db");
         getNextActivity(AuthorizationActivity.class);
      }
   }

   @Override
   protected void onStop() {
      super.onStop();
   }

   private void getNextActivity(Class clazz){
      Intent intent = new Intent(this, clazz);
      startActivity(intent);
      finish();
   }
}
