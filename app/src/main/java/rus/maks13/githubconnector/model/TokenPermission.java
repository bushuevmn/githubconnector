package rus.maks13.githubconnector.model;

import java.util.ArrayList;

public class TokenPermission {

   final ArrayList<String> scopes;
   final String note;
   final String client_id;
   final String client_secret;

   public TokenPermission(ArrayList<String> scopes, String note,
                          String client_id, String client_secret) {
      this.scopes = scopes;
      this.note = note;
      this.client_id = client_id;
      this.client_secret = client_secret;
   }
}
