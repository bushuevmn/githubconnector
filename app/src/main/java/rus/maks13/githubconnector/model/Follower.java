package rus.maks13.githubconnector.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class Follower implements Parcelable {

   private static final String TAG = "FollowerTag";

   @SerializedName("login")
   String login;

   @SerializedName("id")
   long id;

   @SerializedName("url")
   String url;

   @SerializedName("html_url")
   String htmlUrl;

   @SerializedName("avatar_url")
   String avatar;

   @SerializedName("followers")
   int followers;

   @SerializedName("following")
   int following;

   @SerializedName("company")
   String company;

   @SerializedName("name")
   String name;

   @SerializedName("location")
   String city;

   @SerializedName("created_at")
   String joined;

   int starredRepositories;

   public void setAvatar(String avatar) {
      this.avatar = avatar;
   }

   public void setFollowers(int followers) {
      this.followers = followers;
   }

   public void setFollowing(int following) {
      this.following = following;
   }

   public void setCompany(String company) {
      this.company = company;
   }

   public void setName(String name) {
      this.name = name;
   }

   public void setCity(String city) {
      this.city = city;
   }

   public void setStarredRepositories(int starredRepositories) {
      this.starredRepositories = starredRepositories;
   }

   public void setJoined(String joined) {
      this.joined = joined;
   }

   public String getAvatar() {
      return avatar;
   }

   public int getFollowers() {
      return followers;
   }

   public int getFollowing() {
      return following;
   }

   public String getCompany() {
      return company;
   }

   public String getName() {
      return name;
   }

   public String getCity() {
      return city;
   }

   public String getJoined() {
      return joined;
   }

   public int getStarredRepositories() {
      return starredRepositories;
   }

   @Override
   public String toString() {
      return "Follower login=" + login + " id=" + id + " url=" + url + " html_url=" + htmlUrl +
         " name=" + name + " following=" + following + " followers=" + followers
         + " starred=" + starredRepositories + " joined=" + joined
         + "\n";
   }

   public String getLogin() {
      return login;
   }


   // For Parcelable
   protected Follower(Parcel in) {
      login = in.readString();
      id = in.readLong();
      url = in.readString();
      htmlUrl = in.readString();
      avatar = in.readString();
      followers = in.readInt();
      following = in.readInt();
      company = in.readString();
      name = in.readString();
      city = in.readString();
      joined = in.readString();
      starredRepositories = in.readInt();
   }

   public static final Creator<Follower> CREATOR = new Creator<Follower>() {
      @Override
      public Follower createFromParcel(Parcel in) {
         return new Follower(in);
      }

      @Override
      public Follower[] newArray(int size) {
         return new Follower[size];
      }
   };

   @Override
   public int describeContents() {
      return 0;
   }

   @Override
   public void writeToParcel(Parcel dest, int flags) {
      Log.d(TAG, "writeToParcel");
      dest.writeString(login);
      dest.writeLong(id);
      dest.writeString(url);
      dest.writeString(htmlUrl);
      dest.writeString(avatar);
      dest.writeInt(followers);
      dest.writeInt(following);
      dest.writeString(company);
      dest.writeString(name);
      dest.writeString(city);
      dest.writeString(joined);
      dest.writeInt(starredRepositories);
   }
}
