package rus.maks13.githubconnector.model;

import com.google.gson.annotations.SerializedName;

public class Repository {

   @SerializedName("id")
   long id;

   @SerializedName("name")
   String name;

   @SerializedName("full_name")
   String fullName;

   @SerializedName("fork")
   Boolean fork;

   @SerializedName("description")
   String description;

   @SerializedName("updated_at")
   String updated;

   @SerializedName("language")
   String language;

   @SerializedName("forks_count")
   int forksCount;

   @SerializedName("stargezers_count")
   int starredCount;

   @SerializedName("watchers")
   int watchers;

   public long getId() {
      return id;
   }

   public String getName() {
      return name;
   }

   public Boolean getFork() {
      return fork;
   }

   public String getDescription() {
      return description;
   }

   public String getUpdated() {
      return updated;
   }

   public String getLanguage() {
      return language;
   }

   public int getForksCount() {
      return forksCount;
   }

   public int getStarredCount() {
      return starredCount;
   }

   public int getWatchers() {
      return watchers;
   }

   @Override
   public String toString() {
      return "Repository id=" + id + " name=" + name + " full_name=" + fullName
         + " fork=" + fork + " forkCount=" + forksCount + " description=" + description
         + " language=" + language + " starred=" + starredCount + " update=" + updated
         + " watchers=" + watchers
         + "\n";
   }
}
