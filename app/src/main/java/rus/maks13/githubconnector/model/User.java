package rus.maks13.githubconnector.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

@Table(name = "user", id = "id")
public class User extends Model {
   @Column(name = "login")
   @SerializedName("login")
   String login;

   @Column(name = "id")
   @SerializedName("id")
   long id;

   @Column(name = "avatar_url")
   @SerializedName("avatar_url")
   String avatarUrl;

   @Column(name = "type")
   @SerializedName("type")
   String userType;

   @Column(name = "name")
   @SerializedName("name")
   String name;

   @Column(name = "company")
   @SerializedName("company")
   String company;

   @Column(name = "location")
   @SerializedName("location")
   String city;

   @Column(name = "email")
   @SerializedName("email")
   String email;

   @Column(name = "bio")
   @SerializedName("bio")
   String bio;

   @Column(name = "followers")
   @SerializedName("followers")
   int followers;

   @Column(name = "following")
   @SerializedName("following")
   int following;

   @Column(name = "created_at")
   @SerializedName("created_at")
   String createdProfile;

   @Column(name = "disk_usage")
   @SerializedName("disk_usage")
   int diskUsage;

   @Column(name = "starred")
   int starredRepositories;

   public String getLogin() {
      return login;
   }

   public String getAvatarUrl() {
      return avatarUrl;
   }

   public String getUserType() {
      return userType;
   }

   public String getName() {
      return name;
   }

   public String getCompany() {
      return company;
   }

   public String getCity() {
      return city;
   }

   public String getEmail() {
      return email;
   }

   public String getBio() {
      return bio;
   }

   public int getFollowers() {
      return followers;
   }

   public int getFollowing() {
      return following;
   }

   public String getCreatedProfile() {
      return createdProfile;
   }

   public int getDiskUsage() {
      return diskUsage;
   }

   public int getStarredRepositories() {
      return starredRepositories;
   }

   public void setLogin(String login) {
      this.login = login;
   }

   public void setId(long id) {
      this.id = id;
   }

   public void setAvatarUrl(String avatarUrl) {
      this.avatarUrl = avatarUrl;
   }

   public void setUserType(String userType) {
      this.userType = userType;
   }

   public void setName(String name) {
      this.name = name;
   }

   public void setCompany(String company) {
      this.company = company;
   }

   public void setCity(String city) {
      this.city = city;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public void setBio(String bio) {
      this.bio = bio;
   }

   public void setFollowers(int followers) {
      this.followers = followers;
   }

   public void setFollowing(int following) {
      this.following = following;
   }

   public void setCreatedProfile(String createdProfile) {
      this.createdProfile = createdProfile;
   }

   public void setDiskUsage(int diskUsage) {
      this.diskUsage = diskUsage;
   }

   public void setStarredRepositories(int starredRepositories) {
      this.starredRepositories = starredRepositories;
   }

   @Override
   public String toString() {
      return "Full user info\nUser login=" + login + " id=" + id + " avatar=" + avatarUrl
         + " login=" + login + " name=" + name + " company=" + company
         + " city=" + city + " create=" + createdProfile
         + " followers=" + followers + " following=" + following + " starred=" + starredRepositories
         + " User type=" + userType + " email=" + email + " disk usage=" + diskUsage
         + " bio=" + bio;
   }
}
