package rus.maks13.githubconnector.utilites;

public class Constants {

   public static final String API_BASIC_URL = "https://api.github.com";
   public static final String CLIENT_ID = "24664b2cc684dcbe4254";
   public static final String CLIENT_SECRET = "0ead6b72c5cd79d236a4928e9ee7ef4df6fbe045";

   public static final String DATA_BASE_NAME = "gitconnector.db";

   // Fragments name
   public static final String FRAGMENT_FOLLOWERS = "Followers";
   public static final String FRAGMENT_USER_INFO = "User info";
   public static final String FRAGMENT_MY_PROFILE = "My profile";

   // For pass data between fragments
   public static final String PASS_FOLLOWER_INFO = "follower info pass";
   public static final String PASS_USER_INFO = "user info pass";
}
