package rus.maks13.githubconnector.utilites;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import rus.maks13.githubconnector.R;

public class FragmentController implements FragmentManager.OnBackStackChangedListener {

   private static final String TAG = "FragmentController";
   static AppCompatActivity sActivity;
   static FragmentManager sFragmentManager;
   private static TextView mTitle;
   private static FragmentController sFragmentController;
   private static ActionBarDrawerToggle sActionBarDrawerToggle;

   public FragmentController(AppCompatActivity appCompatActivity, TextView textView,
                             ActionBarDrawerToggle toggle) {
      sActivity = appCompatActivity;
      mTitle = textView;
      sFragmentManager = sActivity.getSupportFragmentManager();
      sFragmentManager.addOnBackStackChangedListener(this);
      sActionBarDrawerToggle = toggle;
   }

   public static void initFragmentController(AppCompatActivity appCompatActivity, TextView textView,
                                              ActionBarDrawerToggle toggle) {
      sFragmentController = new FragmentController(appCompatActivity, textView, toggle);
   }


   public FragmentManager getFragmentManager() {
      return sFragmentManager;
   }

   public AppCompatActivity getActivity() {
      return sActivity;
   }

   public static FragmentController getInstance() {
      return sFragmentController;
   }

   public void clearStackAndPushFragment(Fragment fragment, String tag) {
      Fragment fragmentInStack = sFragmentManager.findFragmentByTag(tag);
      if (fragmentInStack == null || !fragmentInStack.isVisible()) {
         Log.d(TAG, "Cleat stack fragments!");
         clearFragmentBackStack();
         pushFragmentToStack(fragment, tag);
      } else {
         Log.d(TAG, "Fragments in stack!");
      }
   }

   public void pushFragmentToStack(Fragment fragment, String tag) {
      FragmentManager manager = sFragmentManager;
      boolean fragmentPopped = manager.popBackStackImmediate(tag, 0);
      if (!fragmentPopped && manager.findFragmentByTag(tag) == null) {
         switch (tag) {

            default: {
               addNewFragmentToStack(manager, fragment, tag);
               break;
            }
         }
      }
   }

   public void clearFragmentBackStack() {
      for (int i = 0; i < sFragmentManager.getBackStackEntryCount(); i++) {
         sFragmentManager.popBackStack();
      }
   }

   private void addNewFragmentToStack(FragmentManager manager, Fragment fragment,
                                      String tag) {
      FragmentTransaction fragmentTransaction = manager.beginTransaction();
      fragmentTransaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out,
         R.anim.slide_left_in, R.anim.slide_left_out);
      fragmentTransaction.replace(R.id.ac_main_frl_fragments, fragment, tag);
      fragmentTransaction.addToBackStack(tag);
      fragmentTransaction.commit();
   }

   @Override
   public void onBackStackChanged() {
      if (sFragmentManager.getBackStackEntryCount() > 1) {
         sActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
         sActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
         sActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      } else {
         sActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
      }
   }

   public void setTitle(String title) {
      mTitle.setText(title);
   }
}
