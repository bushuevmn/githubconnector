package rus.maks13.githubconnector.utilites;

import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.activeandroid.util.Log;

import rus.maks13.githubconnector.db.model.Token;
import rus.maks13.githubconnector.model.User;

public class DataBaseHelper {

   private static final String TAG = "DBHelper";

   public static void connectToDb(Context context) {
      Configuration dbConfig = new Configuration.Builder(context)
         .setDatabaseName(Constants.DATA_BASE_NAME)
         .setModelClasses(Token.class, User.class)
         .create();
      ActiveAndroid.initialize(dbConfig);
   }

   public static boolean disconnectToDB() {
      boolean isDisconnected = false;
      try {
         if (ActiveAndroid.getDatabase().isOpen()) {
            ActiveAndroid.dispose();
            isDisconnected = true;
         }
      } catch (NullPointerException e) {
         Log.d(TAG, "NullPointEx" + e);
      }
      return isDisconnected;
   }

   public static boolean dataBaseIsConnected() {
      try {
         return ActiveAndroid.getDatabase().isOpen();
      } catch (NullPointerException e) {
         return false;
      }
   }

}
