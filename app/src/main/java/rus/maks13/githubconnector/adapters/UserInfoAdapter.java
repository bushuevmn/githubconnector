package rus.maks13.githubconnector.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.model.Follower;
import rus.maks13.githubconnector.model.Repository;

public class UserInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

   private final int USER_VIEW_TYPE = 990;
   private final int REPOSITORY_VIEW_TYPE = 999;
   private Context mContext;
   private ArrayList<Repository> mRepositories;
   private Follower mFollower;
   private DisplayImageOptions mOptions;

   public UserInfoAdapter(Context context, Follower follower, ArrayList<Repository> repositories) {
      mContext = context;
      mFollower = follower;
      mRepositories = repositories;
      mOptions = new DisplayImageOptions.Builder()
         .showImageForEmptyUri(R.drawable.avatar_holder)
         .showImageOnFail(R.drawable.avatar_holder)
         .resetViewBeforeLoading(true)
         .cacheOnDisk(true)
         .imageScaleType(ImageScaleType.EXACTLY)
         .bitmapConfig(Bitmap.Config.RGB_565)
         .considerExifParams(true)
         .displayer(new FadeInBitmapDisplayer(150))
         .build();
   }

   @Override
   public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      switch (viewType) {
         case USER_VIEW_TYPE: {
            View view = LayoutInflater.from(mContext)
               .inflate(R.layout.li_user_full, parent, false);
            return new UserViewHolder(view);
         }
         default: {
            View view = LayoutInflater.from(mContext)
               .inflate(R.layout.li_repository, parent, false);
            return new RepositoryViewHolder(view);
         }
      }
   }

   @Override
   public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
      if (holder instanceof UserViewHolder) {
         UserViewHolder userViewHolder = (UserViewHolder) holder;
         ImageLoader.getInstance().displayImage(mFollower.getAvatar(),
            userViewHolder.avatarImageView, mOptions);
         userViewHolder.loginTextView.setText(mFollower.getLogin());
         if (mFollower.getCity() != null && !mFollower.getCity().isEmpty())
            userViewHolder.cityTextView.setText(mFollower.getCity());
         else
            userViewHolder.cityTextView.setText(mContext.getString(
               R.string.text_holder_not_specified));
         if (mFollower.getCompany() != null && !mFollower.getCompany().isEmpty())
            userViewHolder.companyTextView.setText(mFollower.getCompany());
         else
            userViewHolder.companyTextView.setText(mContext.getString(
               R.string.text_holder_not_specified));
         if (mFollower.getJoined() != null && !mFollower.getJoined().isEmpty())
            userViewHolder.joinedTextView.setText(mFollower.getJoined());
         else
            userViewHolder.joinedTextView.setText(mContext.getString(
               R.string.text_holder_not_specified));
         if (mFollower.getName() != null && !mFollower.getName().isEmpty()) {
            userViewHolder.nameTextView.setVisibility(View.VISIBLE);
            userViewHolder.nameTextView.setText(mFollower.getName());
         } else {
            userViewHolder.nameTextView.setVisibility(View.GONE);
         }
         userViewHolder.followersCountTextView.setText(String.valueOf(mFollower.getFollowers()));
         userViewHolder.starredCountTextView.setText(String.valueOf(
            mFollower.getStarredRepositories()));
         userViewHolder.followingCountTextView.setText(String.valueOf(mFollower.getFollowing()));
      } else if (holder instanceof RepositoryViewHolder) {
         Repository repository = mRepositories.get(position);
         RepositoryViewHolder repositoryViewHolder = (RepositoryViewHolder) holder;
         repositoryViewHolder.nameTextView.setText(repository.getName());
         repositoryViewHolder.languageTextView.setText(repository.getLanguage());
         repositoryViewHolder.starredCountTextView.setText(
            String.valueOf(repository.getStarredCount()));
         repositoryViewHolder.forksCountTextView.setText(String.valueOf(
            repository.getForksCount()));
         repositoryViewHolder.watchCountTextView.setText(String.valueOf(repository.getWatchers()));
         repositoryViewHolder.descriptionTextView.setText(repository.getDescription());
         repositoryViewHolder.updateDateTextView.setText(repository.getUpdated());
         if (repository.getFork()) {
            repositoryViewHolder.forkedInfoContainerLinearLayout.setVisibility(View.VISIBLE);
         } else {
            repositoryViewHolder.forkedInfoContainerLinearLayout.setVisibility(View.GONE);
         }
      }
   }

   @Override
   public int getItemCount() {
      return mRepositories.size();
   }

   @Override
   public int getItemViewType(int position) {
      int typeView = REPOSITORY_VIEW_TYPE;
      if (position == 0) {
         typeView = USER_VIEW_TYPE;
      }
      return typeView;
   }

   private class UserViewHolder extends RecyclerView.ViewHolder {

      CardView userCardView;
      RelativeLayout containerRelativeLayout;
      LinearLayout basicInfoLinearLayout;
      ImageView avatarImageView;
      LinearLayout textInfoContainerLinearLayout;
      TextView loginTextView;
      TextView nameTextView;
      LinearLayout cityInfoContainerLinearLayout;
      ImageView cityImageView;
      TextView cityTextView;
      LinearLayout companyInfoContainerLinearLayout;
      ImageView companyImageView;
      TextView companyTextView;
      LinearLayout joinedInfoContainerLinearLayout;
      ImageView joinedImageView;
      TextView joinedTextView;
      View separatorView;
      LinearLayout additionInfoLinearLayout;
      LinearLayout followersInfoLinearLayout;
      TextView followersCountTextView;
      TextView followersInfoTextView;
      LinearLayout starredInfoLinearLayout;
      TextView starredCountTextView;
      TextView starredInfoTextView;
      LinearLayout followingInfoLinearLayout;
      TextView followingCountTextView;
      TextView followingInfoTextView;

      public UserViewHolder(View itemView) {
         super(itemView);

         userCardView = (CardView) itemView.findViewById(R.id.li_user_full_card_view);
         containerRelativeLayout = (RelativeLayout) itemView.findViewById(
            R.id.li_user_full_rl_container);
         basicInfoLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_basic_info);
         avatarImageView = (ImageView) itemView.findViewById(R.id.li_user_full_iv_avatar);
         textInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_text_info_container);
         loginTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_login);
         nameTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_name);
         cityInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_city_info_container);
         cityImageView = (ImageView) itemView.findViewById(R.id.li_user_full_iv_city_ico);
         cityTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_city);
         companyInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_company_info_container);
         companyImageView = (ImageView) itemView.findViewById(R.id.li_user_full_iv_company_ico);
         companyTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_company);
         joinedInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_joined_info_container);
         joinedImageView = (ImageView) itemView.findViewById(R.id.li_user_full_iv_joined_ico);
         joinedTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_joined);
         separatorView = itemView.findViewById(R.id.li_user_full_vw_separator);
         additionInfoLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_addition_info);
         followersInfoLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_info_followers);
         followersCountTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_followers);
         followersInfoTextView = (TextView) itemView.findViewById(
            R.id.li_user_full_tv_text_followers);
         starredInfoLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_info_starred);
         starredCountTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_starred);
         starredInfoTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_text_starred);
         followingInfoLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_user_full_ll_info_following);
         followingCountTextView = (TextView) itemView.findViewById(R.id.li_user_full_tv_following);
         followingInfoTextView = (TextView) itemView.findViewById(
            R.id.li_user_full_tv_text_following);
      }
   }

   private class RepositoryViewHolder extends RecyclerView.ViewHolder {

      CardView repositoryCardView;
      RelativeLayout containerRelativeLayout;
      TextView nameTextView;
      LinearLayout additionInfoContainerLinearLayout;
      TextView languageTextView;
      ImageView starredImageView;
      TextView starredCountTextView;
      ImageView forksImageView;
      TextView forksCountTextView;
      ImageView watchImageView;
      TextView watchCountTextView;
      TextView descriptionTextView;
      LinearLayout forkedInfoContainerLinearLayout;
      ImageView forkedImageView;
      TextView forkedInfoTextView;
      LinearLayout updateInfoContainerLinearLayout;
      TextView updateInfoTextView;
      TextView updateDateTextView;

      public RepositoryViewHolder(View itemView) {
         super(itemView);

         repositoryCardView = (CardView) itemView.findViewById(R.id.li_repository_card_view);
         containerRelativeLayout = (RelativeLayout) itemView.findViewById(
            R.id.li_repository_rl_container);
         nameTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_name);
         additionInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_repository_ll_addition_info_container);
         languageTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_language);
         starredImageView = (ImageView) itemView.findViewById(R.id.li_repository_iv_starred);
         starredCountTextView = (TextView) itemView.findViewById(
            R.id.li_repository_tv_starred_count);
         forksImageView = (ImageView) itemView.findViewById(R.id.li_repository_iv_forks);
         forksCountTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_forks_count);
         watchImageView = (ImageView) itemView.findViewById(R.id.li_repository_iv_watch);
         watchCountTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_watch_count);
         descriptionTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_description);
         forkedInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_repository_ll_forked_info_container);
         forkedImageView = (ImageView) itemView.findViewById(R.id.li_repository_iv_forked);
         forkedInfoTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_forked_info);
         updateInfoContainerLinearLayout = (LinearLayout) itemView.findViewById(
            R.id.li_repository_ll_update_info_container);
         updateInfoTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_updated_info);
         updateDateTextView = (TextView) itemView.findViewById(R.id.li_repository_tv_updated_date);
      }
   }
}
