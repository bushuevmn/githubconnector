package rus.maks13.githubconnector.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

import rus.maks13.githubconnector.R;
import rus.maks13.githubconnector.model.Follower;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.ViewHolder> {

   private static final String TAG = "FollowerAdapter";
   private ArrayList<Follower> mFollowers;
   private DisplayImageOptions mOptions;
   private Context mContext;

   public FollowersAdapter(Context context, ArrayList<Follower> followers) {
      mFollowers = followers;
      mContext = context;
      mOptions = new DisplayImageOptions.Builder()
         .showImageForEmptyUri(R.drawable.avatar_holder)
         .showImageOnFail(R.drawable.avatar_holder)
         .resetViewBeforeLoading(true)
         .cacheOnDisk(true)
         .imageScaleType(ImageScaleType.EXACTLY)
         .bitmapConfig(Bitmap.Config.RGB_565)
         .considerExifParams(true)
         .displayer(new FadeInBitmapDisplayer(150))
         .build();
   }

   @Override
   public FollowersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      Log.d(TAG, "Create holder");
      View view = LayoutInflater.from(mContext)
         .inflate(R.layout.li_user, parent, false);
      return new ViewHolder(view);
   }

   @Override
   public void onBindViewHolder(FollowersAdapter.ViewHolder holder, int position) {
      Log.d(TAG, "On Bind Holder");
      Follower follower = mFollowers.get(position);
      ImageLoader.getInstance().displayImage(follower.getAvatar(),
         holder.avatarImageView, mOptions);
      holder.loginTextView.setText(follower.getLogin());
      if (follower.getCity() != null && !follower.getCity().isEmpty())
         holder.cityTextView.setText(follower.getCity());
      else
         holder.cityTextView.setText(mContext.getString(R.string.text_holder_not_specified));
      if (follower.getCompany() != null && !follower.getCompany().isEmpty())
         holder.companyTextView.setText(follower.getCompany());
      else
         holder.companyTextView.setText(mContext.getString(R.string.text_holder_not_specified));
      if (follower.getJoined() != null && !follower.getJoined().isEmpty())
         holder.joinedTextView.setText(follower.getJoined());
      else
         holder.joinedTextView.setText(mContext.getString(R.string.text_holder_not_specified));
      holder.followersCountTextView.setText(String.valueOf(follower.getFollowers()));
      holder.starredCountTextView.setText(String.valueOf(follower.getStarredRepositories()));
      holder.followingCountTextView.setText(String.valueOf(follower.getFollowing()));
   }

   @Override
   public int getItemCount() {
      return mFollowers.size();
   }

   public static class ViewHolder extends RecyclerView.ViewHolder {

      CardView userCardView;
      RelativeLayout containerRelativeLayout;
      LinearLayout basicInfoLinearLayout;
      ImageView avatarImageView;
      LinearLayout infoContainerLinearLayout;
      TextView loginTextView;
      LinearLayout cityInfoContainerLinearLayout;
      ImageView cityIcoImageView;
      TextView cityTextView;
      LinearLayout companyInfoContainerLinearLayout;
      ImageView companyIcoImageView;
      TextView companyTextView;
      LinearLayout joinedInfoContainerLinearLayout;
      ImageView joinedIcoImageView;
      TextView joinedTextView;
      View separatorView;
      LinearLayout additionInfoLinearLayout;
      LinearLayout followersInfoLinearLayout;
      TextView followersCountTextView;
      TextView followersInfoTextView;
      LinearLayout starredInfoLinearLayout;
      TextView starredCountTextView;
      TextView starredInfoTextView;
      LinearLayout followingInfoLinearLayout;
      TextView followingCountTextView;
      TextView followingInfoTextView;

      public ViewHolder(View view) {
         super(view);
         userCardView = (CardView) view.findViewById(R.id.li_user_card_view);
         containerRelativeLayout = (RelativeLayout) view.findViewById(R.id.li_user_rl_container);
         basicInfoLinearLayout = (LinearLayout) view.findViewById(R.id.li_user_ll_basic_info);
         avatarImageView = (ImageView) view.findViewById(R.id.li_user_iv_avatar);
         infoContainerLinearLayout = (LinearLayout) view.findViewById(R.id.li_user_ll_info_container);
         loginTextView = (TextView) view.findViewById(R.id.li_user_tv_login);
         cityInfoContainerLinearLayout = (LinearLayout) view.findViewById(
            R.id.li_user_ll_city_info_container);
         cityIcoImageView = (ImageView) view.findViewById(R.id.li_user_iv_city_ico);
         cityTextView = (TextView) view.findViewById(R.id.li_user_tv_city);
         companyInfoContainerLinearLayout = (LinearLayout) view.findViewById(
            R.id.li_user_ll_company_info_container);
         companyIcoImageView = (ImageView) view.findViewById(R.id.li_user_iv_company_ico);
         companyTextView = (TextView) view.findViewById(R.id.li_user_tv_company);
         joinedInfoContainerLinearLayout = (LinearLayout) view.findViewById(
            R.id.li_user_ll_joined_info_container);
         joinedIcoImageView = (ImageView) view.findViewById(R.id.li_user_iv_joined_ico);
         joinedTextView = (TextView) view.findViewById(R.id.li_user_tv_joined);
         separatorView = view.findViewById(R.id.li_user_vw_separator);
         additionInfoLinearLayout = (LinearLayout) view.findViewById(R.id.li_user_ll_addition_info);
         followersInfoLinearLayout = (LinearLayout) view.findViewById(R.id.li_user_ll_info_followers);
         followersCountTextView = (TextView) view.findViewById(R.id.li_user_tv_followers);
         followersInfoTextView = (TextView) view.findViewById(R.id.li_user_tv_text_followers);
         starredInfoLinearLayout = (LinearLayout) view.findViewById(R.id.li_user_ll_info_starred);
         starredCountTextView = (TextView) view.findViewById(R.id.li_user_tv_starred);
         starredInfoTextView = (TextView) view.findViewById(R.id.li_user_tv_text_starred);
         followingInfoLinearLayout = (LinearLayout) view.findViewById(R.id.li_user_ll_info_following);
         followingCountTextView = (TextView) view.findViewById(R.id.li_user_tv_following);
         followingInfoTextView = (TextView) view.findViewById(R.id.li_user_tv_text_following);
      }
   }
}
